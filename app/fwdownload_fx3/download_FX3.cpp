/* C++ application to download firmware to RAM, EEPROM or SPI using C++ CyAPI.lib */

#include "stdafx.h"
#include <windows.h>
#include "CyAPI.h"
#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

/* List of supported programming targets */
typedef enum {
	FW_TARGET_NONE = 0,	/* Invalid target					*/
	FW_TARGET_RAM,		/* Program firmware (hex) to RAM	*/
	FW_TARGET_I2C,		/* Program firmware (hex) to EEPROM	*/
	FW_TARGET_SPI		/* Program firmware (hex) to SPI	*/
} fx3_fw_tgt_p;

/* Timeout (in seconds) for getting a FX3 flash programmer handle */
#define GETHANDLE_TIMEOUT	(5)		

string m_FWDWNLOAD_ERROR_MSG[] = 
{   
	"SUCCESS", 
	"FAILED", 
	"INVALID_MEDIA_TYPE", 
	"INVALID_FWSIGNATURE", 
	"DEVICE_CREATE_FAILED", 
	"INCORRECT_IMAGE_LENGTH", 
	"INVALID_FILE", 
	"SPI_FLASH_ERASE_FAILED", 
	"CORRUPT_FIRMWARE_IMAGE_FILE",
	"I2C_EEPROM_UNKNOWN_SIZE" 
};

/* Function to display usage information */
static void
fx3_dnld_print_usage ( const char *arg0)
{
	printf ("%s: FX3 firmware programmer\n", arg0);
	printf ("Usage:\n");
	printf ("\t%s -h: Print usage information\n\n", arg0);
	printf ("\t%s -i <filename> -t <target>: Program firmware from <filename> to <target>,\n", arg0);
	printf ("\t\twhere <target> is one of:\n");
	printf ("\t\t\t\"RAM \": Program to RAM\n");
	printf ("\t\t\t\"I2C\": Program to I2C EEPROM\n");
	printf ("\t\t\t\"SPI\": Program to SPI FLASH\n");
	printf ("\n");
}

int main (
		  int argc,
		  char *argv[])
{
	FX3_FWDWNLOAD_ERROR_CODE dwld_status = FAILED;
	fx3_fw_tgt_p tgt = FW_TARGET_NONE;
	const char *tgt_str  = NULL;
	char *filename = NULL;
	char *filepath = NULL;
	FILE *fw_img_p = NULL;
	bool flag = false;
	bool status = false;
	int count = 0;
	int fileSz = 0;

	USHORT vendor_id = 0x04B4;
	USHORT bootLoader_product_id = 0x00F3;
	USHORT bootProgrammer_product_id = 0x4720;

	/* Parse command line arguments. */
	for (count = 1; count < argc; count++) 
	{
		if ((_stricmp (argv[count], "-h") == 0) || (_stricmp (argv[count], "--help") == 0))
		{
			fx3_dnld_print_usage (argv[0]);
			return 0;
		} 
		else 
		{
			if ((_stricmp (argv[count], "-t") == 0) || (_stricmp (argv[count], "--target") == 0))
			{
				if (argc > (count + 1)) 
				{
					tgt_str = argv[count + 1];
					if (_stricmp (argv[count + 1], "ram") == 0)
						tgt = FW_TARGET_RAM;
					if (_stricmp (argv[count + 1], "i2c") == 0)
						tgt = FW_TARGET_I2C;
					if (_stricmp (argv[count + 1], "spi") == 0)
						tgt = FW_TARGET_SPI;
					if (tgt == FW_TARGET_NONE)
					{
						fprintf (stderr, "Error: Unknown target %s\n", argv[count + 1]);
						fx3_dnld_print_usage (argv[0]);
						return 0;
					}
				}
				count++;
			} 
			else 
			{
				if ((strcmp (argv[count], "-i") == 0) || (strcmp (argv[count], "--image") == 0)) 
				{
					if (argc > (count + 1))
						filename = argv[count + 1];
					count++;
				} 
				else 
				{
					fprintf (stderr, "Error: Unknown parameter %s\n", argv[count]);
					fx3_dnld_print_usage (argv[0]);
					return 0;
				}
			}
		}
	}
	if ((filename == NULL) || (tgt == FW_TARGET_NONE)) 
	{
		fprintf (stderr, "Error: Firmware binary or target not specified\n");
		fx3_dnld_print_usage (argv[0]);
		return 0;
	}

	int error = fopen_s(&fw_img_p, filename, "rb");
	filepath = filename;

	if (fw_img_p == NULL)
	{
		fprintf (stderr, "Error: Failed to open file\n");
		return false;
	}
	else
	{
		/* Find the length of the image */
		fseek (fw_img_p, 0, SEEK_END);
		fileSz = ftell(fw_img_p);
	}

	if (fileSz <= 0)
	{
		fclose (fw_img_p);
		fprintf (stderr, "Error: Invalid file with length = ZERO\n");
		return false;
	}
	else
	{   
		fclose (fw_img_p);
	}

	/* Start the USB device and acquire an handle for it. */
	CCyFX3Device *m_usbDevice = new CCyFX3Device();
	if (m_usbDevice == NULL)
	{
		fprintf (stderr, "Error: Failed to create USB Device\n");
		return false;
	}

	for (int i = 0; i < m_usbDevice->DeviceCount(); i++)
	{
		if (m_usbDevice->Open((UCHAR)i))
		{
			if (m_usbDevice->VendorID == vendor_id)
			{
				if(m_usbDevice->ProductID == bootLoader_product_id)
				{
					/* We have to look for a device that supports the firmware download vendor commands. */
					if (m_usbDevice->IsBootLoaderRunning ())
					{

						if((tgt == FW_TARGET_I2C)||(tgt == FW_TARGET_SPI))
						{
							printf("Info : Found FX3 USB BootLoader instead of Flash Programmer\n");
							flag = false;
						}
						else
						{
							printf("Info : Found FX3 USB BootLoader\n");
							flag = true ;
						}
						break ;
					}
				}

				if((m_usbDevice->ProductID == bootProgrammer_product_id) && 
					((tgt == FW_TARGET_I2C)||(tgt == FW_TARGET_SPI)))
				{
					flag = true ;
					printf("Info : Found FX3 Flash Programmer\n");
					break ;
				}
			}

			m_usbDevice->Close() ;
		}
	}

	if (flag == false)
	{
		if(tgt == FW_TARGET_RAM)
			fprintf (stderr, "Error: FX3 USB BootLoader Device not found \n");
		else
		{
			//fprintf (stderr, "Error: FX3 Flash Programmer Device not found \n");
			fprintf (stderr, "Error: For downloading firmware image to %s ,first download Flash Programmer image into RAM\n", tgt_str);
			fprintf (stderr, "Info : Flash Programmer device is required for downloading image to %s \n", tgt_str);
			fprintf (stderr, "Note : Flash Programmer image is available in <SuiteUSB Install Path>\\bin directory \n");
		}

		if (m_usbDevice) 
			delete m_usbDevice;
		return false;
	}

	switch (tgt)
	{
	case FW_TARGET_RAM:
		printf("Info : Downloading firmware image into internal RAM \n");
		dwld_status = m_usbDevice->DownloadFw(filepath,RAM);
		break;
	case FW_TARGET_I2C: 
	case FW_TARGET_SPI: 
		printf ("Info : FX3 firmware programming to %s started. Please wait...\n", tgt_str);
		dwld_status = m_usbDevice->DownloadFw(filepath,(tgt == FW_TARGET_I2C)?I2CE2PROM:SPIFLASH);
		break;

	default:
		fprintf (stderr, "Error: Invalid Media type\n");
		return status;
	}

	if(dwld_status == SUCCESS)
	{
		printf ("Info : Programming to %s completed\n", tgt_str);
	}
	else
	{
		std::stringstream sstr;
		sstr.str("");
		sstr << "\nError: Firmware download failed unexpectedly with error code: " <<m_FWDWNLOAD_ERROR_MSG[dwld_status];
		std::cout<< sstr.str() << std::endl;
		if(tgt == FW_TARGET_SPI)
		{
			printf ("Info : Please verify the connection to external SPI device\n");
			printf ("Note : CYUSB3KIT-003 EZ-USB FX3 SuperSpeed Explorer Kit does not have onboard SPI FLASH\n");
		}
	}

	if(m_usbDevice)
	{
		m_usbDevice->Close() ;
		delete m_usbDevice ;
	}
	return 0;
}

