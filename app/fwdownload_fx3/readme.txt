
  FwDownloadApp.exe
  ----------------

  Sample C++ Console Application to program the firmware binary onto FX3 device. 
  The firmware image can be programmed to RAM, I2C EEPROM or SPI FALSH. 

  Usage:
        FwDownloadApp.exe -h: Print usage information

        FwDownloadApp.exe -i <filename> -t <target>

                where <filename> is firmware image in IMG format.
		      <target> is one of the following:
                        "RAM ": Program to RAM
                        "I2C" : Program to I2C EEPROM
                        "SPI" : Program to SPI FLASH

  Example:
  FwDownloadApp.exe -i BulkLoopAuto.img -t RAM

[]
