
  FwDownloadApp.exe
  ----------------

  Sample C++ Console Application to program the firmware binary onto FX2 device. 
  The firmware image can be programmed to RAM, small or large I2C EEPROM. 

Usage:
        FwDownloadApp.exe -h: Print usage information

        FwDownloadApp.exe -i <filename> -t <target>: Program firmware from <filename> to <target>,
                where <target> is one of:
                        "RAM ": Program to internal or external RAM
                        "SI2C": Program to small I2C EEPROM, IIC file to be provided
                        "LI2C": Program to large I2C EEPROM, IIC file to be provided

  Example:
  FwDownloadApp.exe -i BulkLoopAuto.img -t RAM

[]
